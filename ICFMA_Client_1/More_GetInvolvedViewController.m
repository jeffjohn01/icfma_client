//
//  More_GetInvolvedViewController.m
//  ICFMA_Client_1
//
//  Created by Jeffrey Johnston on 5/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "More_GetInvolvedViewController.h"

@interface More_GetInvolvedViewController ()

@end

@implementation More_GetInvolvedViewController

@synthesize involved;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    involved.scalesPageToFit = YES;    
    NSString *urlAddress = @"http://www.savingcranes.org/get-involved.html";    
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [involved loadRequest:requestObj];

    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
