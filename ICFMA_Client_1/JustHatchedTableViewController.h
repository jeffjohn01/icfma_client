//
//  JustHatchedTableViewController.h
//  ICFMA_Client_1
//
//  Created by Jeffrey Johnston on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JHArticleViewController.h"

@interface JustHatchedTableViewController : UITableViewController  {
    
    
    NSMutableArray              *imageArray;
    UIImageView                 *headerView;
    CGPoint                     gestureStartPoint;
    NSUInteger                  indexToShow;
    
    NSArray                     *articles;
    NSString                    *articleTitle;
    NSString                    *articleDate;
    NSString                    *articleText;
    NSString                    *detailTitle;
    NSString                    *detailText;
    NSString                    *imagePath;
    NSMutableData               *receivedData;
    UIImage                     *detailImage;
 
    
    int articleCount;
    
}

@property (nonatomic, retain) IBOutlet UIImageView          *headerView;
@property (nonatomic, retain) UILabel                       *headerLabel;
@property (nonatomic, retain) NSMutableArray                *imageArray;

@property (nonatomic, assign) CGPoint                       gestureStartPoint;
@property (nonatomic, assign) NSUInteger                    indexToShow;

@property (nonatomic, retain) NSArray                       *articles;
@property (nonatomic, retain) NSString                      *articleTitle;
@property (nonatomic, retain) NSString                      *articleText;
@property (nonatomic, retain) NSString                      *detailTitle;
@property (nonatomic, retain) NSString                      *detailText;
@property (nonatomic, retain) NSString                      *articleDate;
@property (nonatomic, retain) NSString                      *imagePath;
@property (nonatomic, retain) NSString                      *fullPath;
@property (nonatomic, assign) int                           articleCount;
@property (nonatomic, retain) NSMutableData                 *receivedData;
@property (nonatomic, retain) UIImage                       *detailImage;
@property (nonatomic, retain) NSString                      *sandURL;
@property (nonatomic, retain) UIImage                       *articleImage;

@property (nonatomic, strong) NSMutableDictionary          *thumbnailsCache;




- (IBAction)handleSwipe:(UISwipeGestureRecognizer *)sender;
- (void) insert; 

@end
