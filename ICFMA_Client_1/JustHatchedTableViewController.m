//
//  JustHatchedTableViewController.m
//  ICFMA_Client_1
//
//  Created by Jeffrey Johnston on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JustHatchedTableViewController.h"
#import <QuartzCore/QuartzCore.h>

#define kMinimumGestureLength (1.0)
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define ktestURL [NSURL URLWithString: @"http://sandhill.cloudaccess.net/sandhil1test1.php"] 
//#define kimageURL [NSURL URLWithString: @"http://sandhill.cloudaccess.net/sandhil1test1.php"] 

@implementation JustHatchedTableViewController


@synthesize imageArray;
@synthesize headerView;
@synthesize headerLabel;
@synthesize gestureStartPoint;
@synthesize indexToShow;
@synthesize articles;
@synthesize articleCount;
@synthesize articleTitle;
@synthesize articleDate;
@synthesize articleText;
@synthesize detailText;
@synthesize detailTitle;
@synthesize imagePath;
@synthesize fullPath;
@synthesize receivedData;
@synthesize detailImage;
@synthesize sandURL;
@synthesize articleImage;

@synthesize thumbnailsCache;



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
         [self.navigationController setNavigationBarHidden:YES animated:NO];
    }
    return self;
}



-(void) handleSwipeFrom: (UISwipeGestureRecognizer *) recognizer {
    
    
}


- (void)insert {
    
    NSData* data = [NSData dataWithContentsOfURL: ktestURL];
    
    NSError *e = nil;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&e];
    
    articles = [json allValues];
    articleCount = [articles count];
    
}




- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    indexToShow = 0;
    imageArray = [NSMutableArray arrayWithObjects:
                  [UIImage imageNamed:@"Image1.png"],
                  [UIImage imageNamed:@"Image2.png"],
                  [UIImage imageNamed:@"Image3.png"],
                  [UIImage imageNamed:@"Image4.png"], nil];
    
    
    headerView.image = (UIImage *) [imageArray objectAtIndex: indexToShow];
    //headerLabel.text = @"This is a test label";
    
    
    UILabel  *testlabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 115, 310, 30)];
    testlabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.5];

    [testlabel setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:16]];
    testlabel.textColor = [UIColor whiteColor];
    testlabel.text = @"  ICF Working To Save Precious Ecosystems";
    
    [headerView addSubview:testlabel];
    
 
    // listen for right swipe
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionRight;
    [self.headerView addGestureRecognizer:swipeGesture];
  
    
    // listen for left swipe
    swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector (handleSwipe:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.headerView addGestureRecognizer:swipeGesture];
   
    
    
}



- (IBAction)handleSwipe:(UISwipeGestureRecognizer *)sender {
    
    CATransition *animation = [CATransition animation];
    UISwipeGestureRecognizerDirection direction = [(UISwipeGestureRecognizer *)sender direction];	
    switch (direction) {
            
            // ignore up, down
        case UISwipeGestureRecognizerDirectionUp:
        case UISwipeGestureRecognizerDirectionDown:
            break;
            
            // left
        case UISwipeGestureRecognizerDirectionLeft:
            self.indexToShow = (self.indexToShow + 1) % [self.imageArray count];
            animation.duration = 0.5;
            animation.type = kCATransitionPush;
            animation.subtype = kCATransitionFromRight;
            [self.headerView.layer addAnimation:animation forKey:@"imageTransition"];
            break;
            
            // right
        case UISwipeGestureRecognizerDirectionRight:
            self.indexToShow = (self.indexToShow + [self.imageArray count] - 1) % [self.imageArray count];
            animation.duration = 0.5;
            animation.type = kCATransitionPush;
            animation.subtype = kCATransitionFromLeft;
            [self.headerView.layer addAnimation:animation forKey:@"imageTransition"];
            break;           
    }
    
    // update image
    //self.headerView.image = [UIImage imageNamed:[self.imageArray objectAtIndex:self.indexToShow]];  
    self.headerView.image = (UIImage *) [self.imageArray objectAtIndex: self.indexToShow];
}




- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self.navigationController setNavigationBarHidden:YES animated:NO];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    [self insert];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return articleCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"JustHatchedCell";
    sandURL = @"http://sandhill.cloudaccess.net/";
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

    }

    
    articleTitle = [[self.articles valueForKey:@"title"] objectAtIndex:indexPath.row];    
    articleText = [[self.articles valueForKey:@"introtext"] objectAtIndex:indexPath.row];
    articleDate = [[self.articles valueForKey:@"created"] objectAtIndex:indexPath.row];
    
    
    
    //
    //GETS IMAGE PATH VIA THE 'CREATED_BY_ALIAS" FIELD IN JOOMLA DB FOR ARTICLE
    //ADDS NAME TO THE SITE URL TO GET FULL PATH TO IMAGE
    //THIS NEEDS TO BE MANUALLY PUT INTO THIS FIELD FOR EACH 'MAIN' ARTICLE IMAGE
    ////////////////////////////////////////////////////////////////////////////////////////////////
    imagePath = [[self.articles valueForKey:@"created_by_alias"] objectAtIndex:indexPath.row];
    NSLog(@"IMAGE PATH:    %@", imagePath);
    if (imagePath != nil) {
        fullPath = [NSString stringWithFormat:@"%@%@", sandURL, imagePath];
        NSLog(@"FULL PATH:    %@", fullPath);
    }
    
    
    
    // Reformatting the date -- NOT WORKING
    ////////////////////////////////////////////////////////////////////////////////////////////////
    NSLog(@"DATE CREATED:      %@", articleDate);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM dd, yyyy - hh:mm:ss a"];
    NSDate *newDate = [formatter dateFromString:articleDate];
    NSString *revDate = [formatter stringFromDate:newDate];
    NSLog(@"NEW DATE:    %@", revDate);
    //articleDate = newDate;
    
    
    
    // Removing HTML tags from articleText
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString:articleText];
    
    while (![theScanner isAtEnd]) 
    {
        [theScanner scanUpToString:@"<" intoString:NULL] ; 
        [theScanner scanUpToString:@">" intoString:&text] ;
        articleText = [articleText stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    articleText = [articleText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
// AN ATTEMPT AT CACHEING THE IMAGES IN A DICTIONARY
///////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    NSString *thumbnailCacheKey = [NSString stringWithFormat:@"cache%d", indexPath.row];
    
    if (![[self.thumbnailsCache allKeys] containsObject:thumbnailCacheKey]) {
        
        // thumbnail for this row is not found in cache, so get it from remote website
        __block NSData *image = nil;        
        dispatch_queue_t imageQueue = dispatch_queue_create("queueForCellImage", NULL);
        dispatch_async(imageQueue, ^{
            //NSString *thumbnailURL = myCustomFunctionGetThumbnailURL:indexPath.row;
            image = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:fullPath]];
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.imageView.image = [UIImage imageWithData:image];
                [self.thumbnailsCache setObject:image forKey:thumbnailCacheKey];
            });
        });
        dispatch_release(imageQueue);
        //[self.thumbnailsCache setObject:image forKey:thumbnailCacheKey];
        
    } else {
        
        // thumbnail is in cache
        NSData *image = [self.thumbnailsCache objectForKey:thumbnailCacheKey];
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.imageView.image = [UIImage imageWithData:image];
        });
        
    }
 //////////////////////////////////////////////////////////////////////////////////////////////////////
*/
    
    

//SIMPLE GCD FOR GETTING IMAGES IN BACKGROUND
////////////////////////////////////////////////////////////////////////////////////////////////////////
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    dispatch_async(queue, ^{
        NSData *imageUrl = [NSData dataWithContentsOfURL:[NSURL URLWithString:fullPath]];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            articleImage = [UIImage imageWithData:imageUrl];
            [[cell imageView] setImage:articleImage];
            [cell setNeedsLayout];
        });
    });
//////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    // Assigning Title, Text to cell
    cell.textLabel.text = articleTitle;
    cell.detailTextLabel.text = articleText;
    
 return cell;
}



//////////////////////////////////



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.

    JHArticleViewController  *articleViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"JHArticle"];
    //DetailArticleViewController *detailViewController = [[DetailArticleViewController alloc] initWithNibName:@"Detials" bundle:nil];
    
    detailTitle = [[self.articles valueForKey:@"title"] objectAtIndex:indexPath.row];    
    detailText = [[self.articles valueForKey:@"introtext"] objectAtIndex:indexPath.row];
    imagePath = [[self.articles valueForKey:@"created_by_alias"] objectAtIndex:indexPath.row];
    
    NSLog(@"IMAGE PATH:    %@", imagePath);
    if (imagePath != nil) {
        fullPath = [NSString stringWithFormat:@"%@%@", sandURL, imagePath];
        NSLog(@"FULL PATH:    %@", fullPath);
    }


    NSData *imageUrl = [NSData dataWithContentsOfURL:[NSURL URLWithString:fullPath]];
    detailImage = [UIImage imageWithData:imageUrl];
   
    
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString:detailText];
    
    while (![theScanner isAtEnd]) 
    {
        
        [theScanner scanUpToString:@"<" intoString:NULL] ; 
        [theScanner scanUpToString:@">" intoString:&text] ;
        detailText = [detailText stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    
    detailText = [detailText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    articleViewController.theImage = detailImage;
    articleViewController.theTitle = detailTitle;
    articleViewController.theBody = detailText;
    
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:articleViewController animated:YES];
    
    
}

@end



/*
 //
 // Create the request.
 NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:fullPath]
 cachePolicy:NSURLRequestUseProtocolCachePolicy
 timeoutInterval:10.0];
 // create the connection with the request
 // and start loading the data
 NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
 if (theConnection) {
 // Create the NSMutableData to hold the received data.
 // receivedData is an instance variable declared elsewhere.
 receivedData = [NSMutableData data];
 NSLog(@"RECEIVED DATA IS:         %@", receivedData);
 } else {
 // Inform the user that the connection failed.
 }
 
 articleImage = [[UIImage alloc] initWithData:receivedData];
 //[cell.imageView setImage:cellImage];
 */



/*
 -(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
 if ([[segue identifier] isEqualToString:@"JHArticle"]) {
 
 NSIndexPath *selectedRowIndex = [self.tableView indexPathForSelectedRow];
 JHArticleViewController *jhArticleViewController = [segue destinationViewController];
 //detailViewController.category_title = [maincategories_array objectAtIndex:selectedRowIndex.row];
 
 }
 }
 */



//cell.imageView   newImageView;
//[cell.imageView setBounds:CGRectMake(0, 0, 50, 50)];
//[cell.imageView setFrame:CGRectMake(0, 0, 50, 50)];
//cell.imageView.autoresizingMask = ( UIViewAutoresizingNone );
//cell.imageView.autoresizesSubviews = NO;

//CGRect imageViewFrame = cell.imageView.frame;
//imageViewFrame.size.width -= 50.0f;
//cell.imageView.frame = imageViewFrame;

//cell.imageView.bounds = CGRectMake(0, 0, 50, 50);
//cell.imageView.frame = CGRectMake(0, 0, 50, 50);














