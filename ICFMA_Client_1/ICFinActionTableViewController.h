//
//  ICFinActionTableViewController.h
//  ICFMA_Client_1
//
//  Created by Jeffrey Johnston on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICFArticleViewController.h"

@interface ICFinActionTableViewController : UITableViewController {
    
    
    NSMutableArray              *actionImageArray;
    UIImageView                 *actionHeaderView;
    CGPoint                     gestureStartPoint;
    NSUInteger                  indexToShow;
    
    NSArray                     *actionArticles;
    NSString                    *actionArticleTitle;
    NSString                    *actionArticleDate;
    NSString                    *actionArticleText;
    NSString                    *actionDetailTitle;
    NSString                    *actionDetailText;
    NSString                    *actionImagePath;
    NSMutableData               *receivedData;
    UIImage                     *actionDetailImage;
    
    int articleCount;

}

@property (nonatomic, retain) IBOutlet UIImageView          *actionHeaderView;
@property (nonatomic, retain) NSMutableArray                *actionImageArray;

@property (nonatomic, assign) CGPoint                       gestureStartPoint;
@property (nonatomic, assign) NSUInteger                    indexToShow;

@property (nonatomic, retain) NSArray                       *actionArticles;
@property (nonatomic, retain) NSString                      *actionArticleTitle;
@property (nonatomic, retain) NSString                      *actionArticleText;
@property (nonatomic, retain) NSString                      *actionDetailTitle;
@property (nonatomic, retain) NSString                      *actionDetailText;
@property (nonatomic, retain) NSString                      *actionArticleDate;
@property (nonatomic, retain) NSString                      *actionImagePath;
@property (nonatomic, retain) NSString                      *actionFullPath;
@property (nonatomic, assign) int                           articleCount;
@property (nonatomic, retain) NSMutableData                 *receivedData;
@property (nonatomic, retain) UIImage                       *actionDetailImage;
@property (nonatomic, retain) NSString                      *sandURL;
@property (nonatomic, retain) UIImage                       *actionArticleImage;




-(IBAction)handleSwipe:(UISwipeGestureRecognizer *) recognizer;

-(void) insert; 

@end
