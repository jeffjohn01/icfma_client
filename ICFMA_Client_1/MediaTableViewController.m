//
//  MediaTableViewController.m
//  ICFMA_Client_1
//
//  Created by Jeffrey Johnston on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MediaTableViewController.h"
#import "YouTubeDetailViewController.h"


@interface MediaTableViewController (PrivateMethods)
- (GDataServiceGoogleYouTube *)youTubeService;

@end


@implementation MediaTableViewController

@synthesize feed;

- (void)awakeFromNib
{
    [super awakeFromNib];
}





- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
}



- (void)viewDidLoad {
	NSLog(@"loading");
    
	GDataServiceGoogleYouTube *service = [self youTubeService];
    
	NSString *uploadsID = kGDataYouTubeUserFeedIDUploads;
	NSURL *feedURL = [GDataServiceGoogleYouTube youTubeURLForUserID:@"ICFCraneTube"
														 userFeedID:uploadsID];
	
	[service fetchFeedWithURL:feedURL
                     delegate:self
            didFinishSelector:@selector(request:finishedWithFeed:error:)];
    
    [super viewDidLoad];	
}

- (void)request:(GDataServiceTicket *)ticket
finishedWithFeed:(GDataFeedBase *)aFeed
          error:(NSError *)error {
    
	self.feed = (GDataFeedYouTubeVideo *)aFeed;
    
	[self.tableView reloadData];
}




#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[feed entries] count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"YouTubeCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
	// Configure the cell.
	GDataEntryBase *entry = [[feed entries] objectAtIndex:indexPath.row];
	NSString *title = [[entry title] stringValue];
    NSString *description = [entry description];
    
    NSArray *thumbnails = [[(GDataEntryYouTubeVideo *)entry mediaGroup] mediaThumbnails];
    
    
    NSLog(@"CONTENT:    %@", description);
	cell.textLabel.text = title;
    cell.detailTextLabel.text = description;
	
	
    dispatch_queue_t downloadQueue = dispatch_queue_create("image downloader", NULL);
    dispatch_async(downloadQueue, ^{
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[thumbnails objectAtIndex:0] URLString]]];
        UIImage * image = [UIImage imageWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{            
            cell.imageView.image = image;
            cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
            [cell setNeedsLayout];
        });
    });
	
    return cell;
}




- (GDataServiceGoogleYouTube *)youTubeService {
	static GDataServiceGoogleYouTube* _service = nil;
	
	if (!_service) {
		_service = [[GDataServiceGoogleYouTube alloc] init];
		
		[_service setUserAgent:@"MyApp-UserApp-1.0"];
		//[_service setShouldCacheDatedData:YES];
        [_service setShouldCacheResponseData:YES];
		[_service setServiceShouldFollowNextLinks:YES];
	}
	
	// fetch unauthenticated
	[_service setUserCredentialsWithUsername:nil
                                    password:nil];
	
	return _service;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    YouTubeDetailViewController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:@"YouTubeDetailViewController"];
    
    
    GDataEntryBase *entry2 = [[feed entries] objectAtIndex:indexPath.row];
    NSString *title = [[entry2 title] stringValue];
    NSArray *contents = [[(GDataEntryYouTubeVideo *)entry2 mediaGroup] mediaContents];
    GDataMediaContent *flashContent = [GDataUtilities firstObjectFromArray:contents withValue:@"application/x-shockwave-flash" forKeyPath:@"type"];
    NSString *tempURL = [flashContent URLString];
    //NSLog(@"The URL is:%@",tempURL);
    //NSString *test = @"This is a test";
    detailController.videoString = tempURL;
    detailController.titleString = title;
    
    // [self embedYouTube:[flashContent URLString] frame:CGRectMake(70, 100, 200, 200)];
    [self.navigationController pushViewController:detailController animated:YES];
    
}




@end