//
//  ICFArticleViewController.h
//  ICFMA_Client_1
//
//  Created by Jeffrey Johnston on 5/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICFArticleViewController : UIViewController  {
    
    UILabel         *actionArticleHeadline;
    UILabel         *actionArticleDateAndAuthor;
    UIImageView     *actionArticleImage;
    UITextView      *actionArticleBody;
    
    NSString        *theTitle;
    NSString        *theBody;
    UIImage         *theImage;
    
    
}


@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UILabel *actionArticleHeadline;
@property (nonatomic, retain) IBOutlet UITextView *actionArticleBody;
@property (nonatomic, retain) IBOutlet UIImageView *actionArticleImage;

@property (nonatomic, retain) NSString *theTitle;
@property (nonatomic, retain) NSString *theBody;
@property (nonatomic, retain) UIImage  *theImage;

@property (nonatomic, retain) IBOutlet UILabel *actionArticleDateAndAuthor;




@end
