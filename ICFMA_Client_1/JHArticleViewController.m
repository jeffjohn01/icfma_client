//
//  JHArticleViewController.m
//  ICFMA_Client_1
//
//  Created by Jeffrey Johnston on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JHArticleViewController.h"
#import "JustHatchedTableViewController.h"

@interface JHArticleViewController ()

@end

@implementation JHArticleViewController

@synthesize jhArticleHeadline;
@synthesize jhArticleDateAndAuthor;
@synthesize jhArticleImage;
@synthesize jhArticleBody;
@synthesize scrollView;
@synthesize theBody;
@synthesize theTitle;
@synthesize theImage;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.navigationController setNavigationBarHidden:NO animated:YES];
            
    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated {
    
    //[self.view addSubview:jhArticleBody];
    self.jhArticleBody.text = theBody;
    //CGRect frame = jhArticleBody.frame;
    //frame.size.height = jhArticleBody.contentSize.height;
    //jhArticleBody.frame = frame;
    self.jhArticleHeadline.text = theTitle;
    
    [self.jhArticleImage setImage:theImage];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];    
    
}



- (void)viewDidLoad
{
    [super viewDidLoad];

    //self.navigationController.navigationBarHidden = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, 1800);

}

- (void)viewDidUnload
{

    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self setJhArticleHeadline:nil];
    [self setJhArticleDateAndAuthor:nil];
    [self setJhArticleImage:nil];
    [self setJhArticleBody:nil];
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
