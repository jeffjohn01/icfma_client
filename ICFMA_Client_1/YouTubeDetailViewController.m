//
//  YouTubeDetailViewController.m
//  ICFMA_Client_1
//
//  Created by Jeff Johnston on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "YouTubeDetailViewController.h"

@interface YouTubeDetailViewController ()
- (void)configureView;
@end

@implementation YouTubeDetailViewController

@synthesize webView;
@synthesize detailDescriptionLabel;
@synthesize videoString, titleString;


#pragma mark - Managing the detail item
- (void) displayGoogleVideo:(NSString *)urlString frame:(CGRect)frame  
{
    NSString *htmlString = [NSString stringWithFormat:@"<html><head><meta name = \"viewport\" content = \"initial-scale = 1.0, user-scalable = no, width = 212\"/></head><body style=\"background:#F00;margin-top:0px;margin-left:0px\"><div><param name=\"movie\" value=\"%@\"></param><param name=\"wmode\" value=\"transparent\"></param><embed src=\"%@\" type=\"application/x-shockwave-flash\" wmode=\"transparent\" width=\"%0.0f\" height=\"%0.0f\"></embed></object></div></body></html>",urlString,urlString,frame.size.width,frame.size.height];
    
    [webView loadHTMLString:htmlString baseURL:nil];
    NSLog(@"HTML IS: %@", htmlString);
    
}
/*
 - (void)embedYouTube:(NSString *)urlString frame:(CGRect)frame {
 NSString *embedHTML = @"\
 <html><head>\
 <style type=\"text/css\">\
 body {\
 background-color: transparent;\
 color: transparent;\
 }\
 </style>\
 </head><body style=\"margin:0\">\
 <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
 width=\"%0.0f\" height=\"%0.0f\"></embed>\
 </body></html>";
 NSString *html = [NSString stringWithFormat:embedHTML, urlString, frame.size.width, frame.size.height];
 NSLog(@"HTML IS: %@", html);
 [webView loadHTMLString:html baseURL:nil];
 
 
 }
 */


- (void)configureView
{
    // Update the user interface for the detail item.
    //NSLog(@"URL is:%@", videoString);
    //[self embedYouTube:videoString frame:CGRectMake(70, 100, 200, 200)];
    [self displayGoogleVideo:videoString frame:CGRectMake(0, 0, 320, 175)];
    detailDescriptionLabel.text = titleString;
    
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"YouTube";
    
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.detailDescriptionLabel = nil;
}
@end
