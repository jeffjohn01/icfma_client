//
//  JHArticleViewController.h
//  ICFMA_Client_1
//
//  Created by Jeffrey Johnston on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JHArticleViewController : UIViewController <UIScrollViewDelegate> {
    
    UILabel         *jhArticleHeadline;
    UILabel         *jhArticleDateAndAuthor;
    UIImageView     *jhArticleImage;
    UITextView      *jhArticleBody;
    
    NSString        *theTitle;
    NSString        *theBody;
    UIImage         *theImage;
}




@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

@property (nonatomic, retain) IBOutlet UILabel *jhArticleHeadline;
@property (nonatomic, retain) IBOutlet UITextView *jhArticleBody;
@property (nonatomic, retain) IBOutlet UIImageView *jhArticleImage;

@property (nonatomic, retain) NSString *theTitle;
@property (nonatomic, retain) NSString *theBody;
@property (nonatomic, retain) UIImage  *theImage;

@property (nonatomic, retain) IBOutlet UILabel *jhArticleDateAndAuthor;



@end
