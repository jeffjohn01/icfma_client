//
//  MediaTableViewController.h
//  ICFMA_Client_1
//
//  Created by Jeffrey Johnston on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GDataYouTube.h"
#import "GDataServiceGoogleYouTube.h"
#import "GData.h"


@interface MediaTableViewController : UITableViewController {
	GDataFeedYouTubeVideo *feed;
}




@property (nonatomic, retain) GDataFeedYouTubeVideo *feed;


@end
