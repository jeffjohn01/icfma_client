//
//  ICFinActionTableViewController.m
//  ICFMA_Client_1
//
//  Created by Jeffrey Johnston on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ICFinActionTableViewController.h"
#import <QuartzCore/QuartzCore.h>


#define kMinimumGestureLength (1.0)
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define ktestURL [NSURL URLWithString: @"http://sandhill.cloudaccess.net/sandhil1test1.php"] 

@implementation ICFinActionTableViewController



@synthesize actionImageArray;
@synthesize actionHeaderView;
@synthesize gestureStartPoint;
@synthesize indexToShow;
@synthesize actionArticles;
@synthesize articleCount;
@synthesize actionArticleTitle;
@synthesize actionArticleDate;
@synthesize actionArticleText;
@synthesize actionDetailText;
@synthesize actionDetailTitle;
@synthesize actionFullPath;
@synthesize actionImagePath;
@synthesize actionDetailImage;
@synthesize sandURL;
@synthesize actionArticleImage;
@synthesize receivedData;




- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void) handleSwipeFrom: (UISwipeGestureRecognizer *) recognizer {
    
    
}


- (void)insert {
    
    NSData* data = [NSData dataWithContentsOfURL: ktestURL];
    
    NSError *e = nil;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&e];
    
    actionArticles = [json allValues];
    articleCount = [actionArticles count];
    
}



- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    indexToShow = 1;
    actionImageArray = [NSMutableArray arrayWithObjects:
                  [UIImage imageNamed:@"Image1.png"],
                  [UIImage imageNamed:@"Image2.png"],
                  [UIImage imageNamed:@"Image3.png"],
                  [UIImage imageNamed:@"Image4.png"], nil];
    
     
    actionHeaderView.image = (UIImage *) [actionImageArray objectAtIndex: indexToShow];

    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionRight;
    [self.actionHeaderView addGestureRecognizer:swipeGesture];
    
    
    // listen for left swipe
    swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector (handleSwipe:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.actionHeaderView addGestureRecognizer:swipeGesture];
    
}


- (IBAction)handleSwipe:(UISwipeGestureRecognizer *)sender {
    
    CATransition *animation = [CATransition animation];
    UISwipeGestureRecognizerDirection direction = [(UISwipeGestureRecognizer *)sender direction];	
    switch (direction) {
            
            // ignore up, down
        case UISwipeGestureRecognizerDirectionUp:
        case UISwipeGestureRecognizerDirectionDown:
            break;
            
            // left
        case UISwipeGestureRecognizerDirectionLeft:
            self.indexToShow = (self.indexToShow + 1) % [self.actionImageArray count];
            animation.duration = 0.5;
            animation.type = kCATransitionPush;
            animation.subtype = kCATransitionFromRight;
            [self.actionHeaderView.layer addAnimation:animation forKey:@"imageTransition"];
            break;
            
            // right
        case UISwipeGestureRecognizerDirectionRight:
            self.indexToShow = (self.indexToShow + [self.actionImageArray count] - 1) % [self.actionImageArray count];
            animation.duration = 0.5;
            animation.type = kCATransitionPush;
            animation.subtype = kCATransitionFromLeft;
            [self.actionHeaderView.layer addAnimation:animation forKey:@"imageTransition"];
            break;           
    }
    
    // update image
    //self.headerView.image = [UIImage imageNamed:[self.imageArray objectAtIndex:self.indexToShow]];  
    self.actionHeaderView.image = (UIImage *) [self.actionImageArray objectAtIndex: self.indexToShow];
}







- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
      [self insert];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return articleCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ICFACell";
    sandURL = @"http://sandhill.cloudaccess.net/";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    actionArticleTitle = [[self.actionArticles valueForKey:@"title"] objectAtIndex:indexPath.row];    
    actionArticleText = [[self.actionArticles valueForKey:@"introtext"] objectAtIndex:indexPath.row];
    actionArticleDate = [[self.actionArticles valueForKey:@"created"] objectAtIndex:indexPath.row];
    
    
    
    
    actionImagePath = [[self.actionArticles valueForKey:@"created_by_alias"] objectAtIndex:indexPath.row];
    //NSLog(@"IMAGE PATH:    %@", imagePath);
    if (actionImagePath != nil) {
        actionFullPath = [NSString stringWithFormat:@"%@%@", sandURL, actionImagePath];
        //NSLog(@"FULL PATH:    %@", fullPath);
    }
    
    /*
     //
     // Create the request.
     NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:fullPath]
     cachePolicy:NSURLRequestUseProtocolCachePolicy
     timeoutInterval:10.0];
     // create the connection with the request
     // and start loading the data
     NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
     if (theConnection) {
     // Create the NSMutableData to hold the received data.
     // receivedData is an instance variable declared elsewhere.
     receivedData = [NSMutableData data];
     NSLog(@"RECEIVED DATA IS:         %@", receivedData);
     } else {
     // Inform the user that the connection failed.
     }
     
     UIImage *cellImage = [[UIImage alloc] initWithData:receivedData];
     [cell.imageView setImage:cellImage];
     */
    
    
    
    
    
    
    
    
    
    
    
    //UIImage *tableImage = [[UIImage alloc] init]; 
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    dispatch_async(queue, ^{
        NSData *imageUrl = [NSData dataWithContentsOfURL:[NSURL URLWithString:actionFullPath]];        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            actionArticleImage = [UIImage imageWithData:imageUrl];
            [[cell imageView] setImage:actionArticleImage];
            [cell setNeedsLayout];
        });
    });

    
    
       
    
               
    

    
    
    
    
    
    
    
    
    // Reformatting the date
    NSLog(@"DATE CREATED:      %@", actionArticleDate);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM dd, yyyy - hh:mm:ss a"];
    NSDate *newDate = [formatter dateFromString:actionArticleDate];
    NSString *revDate = [formatter stringFromDate:newDate];
    NSLog(@"NEW DATE:    %@", revDate);
    //articleDate = newDate;
    
    // Removing HTML tags from articleText
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString:actionArticleText];
    
    while (![theScanner isAtEnd]) 
    {
        [theScanner scanUpToString:@"<" intoString:NULL] ; 
        [theScanner scanUpToString:@">" intoString:&text] ;
        actionArticleText = [actionArticleText stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    actionArticleText = [actionArticleText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    // Assigning Title and Text to cell
    cell.imageView.image = actionArticleImage;
    cell.textLabel.text = actionArticleTitle;
    cell.detailTextLabel.text = actionArticleText;

    
    
    return cell;
}


//////////////////////////////////
/*
 -(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
 if ([[segue identifier] isEqualToString:@"JHArticle"]) {
 
 NSIndexPath *selectedRowIndex = [self.tableView indexPathForSelectedRow];
 JHArticleViewController *jhArticleViewController = [segue destinationViewController];
 //detailViewController.category_title = [maincategories_array objectAtIndex:selectedRowIndex.row];
 
 }
 }
 */








#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
     ICFArticleViewController *articleViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ICFArticle"];    
    
    
    actionDetailTitle = [[self.actionArticles valueForKey:@"title"] objectAtIndex:indexPath.row];    
    actionDetailText = [[self.actionArticles valueForKey:@"introtext"] objectAtIndex:indexPath.row];
    
    actionImagePath = [[self.actionArticles valueForKey:@"created_by_alias"] objectAtIndex:indexPath.row];
    
    //NSLog(@"IMAGE PATH:    %@", imagePath);
    if (actionImagePath != nil) {
        actionFullPath = [NSString stringWithFormat:@"%@%@", sandURL, actionImagePath];
        //NSLog(@"FULL PATH:    %@", fullPath);
    }
    
    NSData *imageUrl = [NSData dataWithContentsOfURL:[NSURL URLWithString:actionFullPath]];
    actionDetailImage = [UIImage imageWithData:imageUrl];

    
    
    
    
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString:actionDetailText];
    
    while (![theScanner isAtEnd]) 
    {
        
        [theScanner scanUpToString:@"<" intoString:NULL] ; 
        [theScanner scanUpToString:@">" intoString:&text] ;
        actionDetailText = [actionDetailText stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    
    actionDetailText = [actionDetailText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    articleViewController.theImage = actionDetailImage;
    articleViewController.theTitle = actionDetailTitle;
    articleViewController.theBody = actionDetailText;
    

    
    
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:articleViewController animated:YES];
     
}

@end







