//
//  More_GetInvolvedViewController.h
//  ICFMA_Client_1
//
//  Created by Jeffrey Johnston on 5/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface More_GetInvolvedViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIWebView *involved;

@end
