//
//  ICFArticleViewController.m
//  ICFMA_Client_1
//
//  Created by Jeffrey Johnston on 5/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ICFArticleViewController.h"

@interface ICFArticleViewController ()

@end

@implementation ICFArticleViewController



@synthesize actionArticleHeadline;
@synthesize actionArticleDateAndAuthor;
@synthesize actionArticleImage;
@synthesize actionArticleBody;
@synthesize scrollView;
@synthesize theBody;
@synthesize theTitle;
@synthesize theImage;












- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
         [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
    return self;
}




- (void)viewWillAppear:(BOOL)animated {
    
    
    self.actionArticleHeadline.text = theTitle;
    self.actionArticleBody.text = theBody;
    [self.actionArticleImage setImage:theImage];
    //[theWeb loadHTMLString:theBody baseURL:nil];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];    
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, 1800);


}

- (void)viewDidUnload
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self setActionArticleHeadline:nil];
    [self setActionArticleDateAndAuthor:nil];
    [self setActionArticleImage:nil];
    [self setActionArticleBody:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
