//
//  YouTubeDetailViewController.h
//  ICFMA_Client_1
//
//  Created by Jeff Johnston on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YouTubeDetailViewController : UIViewController {
    IBOutlet UIWebView *webView;
    IBOutlet UILabel *detailDescriptionLabel;
    NSString *videoString;
    NSString *titleString;
}

@property (strong, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, retain) NSString *videoString;
@property (nonatomic, retain) NSString *titleString;

//- (void)embedYouTube:(NSString *)urlString frame:(CGRect)frame;



@end
