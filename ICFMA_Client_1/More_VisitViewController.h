//
//  More_VisitViewController.h
//  ICFMA_Client_1
//
//  Created by Jeffrey Johnston on 5/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface More_VisitViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIWebView *visit;

@end
